package enums;

public enum TypeOfDocumentGoogleDrive {

    PHOTO("image/jpeg"), DOCUMENT("word/docx");

    private final String type;

    TypeOfDocumentGoogleDrive(String type) {
        this.type = type;
    }

    public String getTypeOfDocument() {
        return type;
    }

}
