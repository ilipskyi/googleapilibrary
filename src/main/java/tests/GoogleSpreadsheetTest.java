package tests;

import services.spreadsheet.GoogleSheetsApi;
import test_data.SpreadSheetTestData;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class GoogleSpreadsheetTest {

    public static void main(String[] args) throws IOException, GeneralSecurityException {

        int sheetRangeCounter = 0;
        GoogleSheetsApi googleSheetsApi = new GoogleSheetsApi();

        googleSheetsApi.getDataFromGoogleSheet(SpreadSheetTestData.getSheetName()
                + SpreadSheetTestData.getRangeToGetDataFrom());
        googleSheetsApi.addDataInGoogleSheet(SpreadSheetTestData.getSheetName()
                + SpreadSheetTestData.getRangeToAddDataTo(), SpreadSheetTestData.getNewPhoneModel());
        googleSheetsApi.updateDataInGoogleSheet(SpreadSheetTestData.getSheetName()
                + SpreadSheetTestData.getRangeToUpdateDataIn(), SpreadSheetTestData.getUpdatedPhonePrice());
        googleSheetsApi.deleteDataInGoogleSheet(SpreadSheetTestData.getSheetRange()[sheetRangeCounter],
                SpreadSheetTestData.getSheetRange()[sheetRangeCounter + 1],
                SpreadSheetTestData.getSheetRange()[sheetRangeCounter + 2]);
        googleSheetsApi.copyDataInGoogleSheet(SpreadSheetTestData.getSheetName()
                        + SpreadSheetTestData.getRangeToCopyDataFrom(),
                SpreadSheetTestData.getSheetName() + SpreadSheetTestData.getRangeToCopyDataTo());
    }
}
