package tests;

import enums.TypeOfDocumentGoogleDrive;
import services.drive.GoogleDriveApi;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static test_data.UploadDownloadFiles.*;

public class GoogleDriveAPITest {

    public static void main(String[] args) throws GeneralSecurityException, IOException {
        GoogleDriveApi googleDriveAPI = new GoogleDriveApi();
        googleDriveAPI.printNamesOfFiles(10);
        googleDriveAPI.downloadFilesFromDrive(getDownloadFileName(), getDownloadDirectory(), getFileId());
        googleDriveAPI.uploadFileToDrive(getUploadFileName(), getUploadDirectory(),
                TypeOfDocumentGoogleDrive.PHOTO);
    }

}
