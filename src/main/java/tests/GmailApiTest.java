package tests;

import services.gmail.GmailApi;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

public class GmailApiTest {

    public static void main(String[] args) throws GeneralSecurityException, IOException, MessagingException {
        GmailApi gmailApi = new GmailApi();
        MimeMessage messageWithAttachmentToSend = GmailApi.createEmailWithAttachment("ivan.lipskyi@qatestlab.eu",
                "me","Test Subject Gmail Api with attachment", "This is test message with attachment",
                new File("./cat.jpg"));
        gmailApi.sendMessage("me", messageWithAttachmentToSend);
        MimeMessage messageToSend = GmailApi.createEmail("ivan.lipskyi@qatestlab.eu", "me",
                "Test Subject Gmail Api", "This is test message");
        gmailApi.sendMessage("me", messageToSend);
        gmailApi.printMessageBody("me", "from: " + "lips.ivan5@gmail.com", 0);
    }

}
