package services.spreadsheet;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.Props;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GoogleSheetsApi extends Authorization {

    private static final Logger Log = LogManager.getLogger(GoogleSheetsApi.class.getName());
    private static Sheets sheetsService;
    private static final String SPREADSHEET_ID = Props.getSpreadsheetId();

    /**
     * Method to extract data from a google spreadsheet
     *  Will take all data from the range you specified and print as a list
     */
    public List<List<Object>> getDataFromGoogleSheet(String range) throws IOException, GeneralSecurityException {
        Log.info("Trying to get data from the google spreadsheet");
        sheetsService = getSheetsService();
        ValueRange response = sheetsService.spreadsheets().values()
                .get(SPREADSHEET_ID, range)
                .execute();

        List<List<Object>> values = response.getValues();
        if (values == null || values.isEmpty()) {
            Log.info("No data found.");
        } else {
            values.forEach(System.out::println);
        }
        Log.info("Data successfully extracted from the google spreadsheet");
        return values;
    }

    private void appendResult(String range, ValueRange appendBody) throws IOException {
        AppendValuesResponse appendResult = sheetsService.spreadsheets().values()
                .append(SPREADSHEET_ID, range, appendBody)
                .setValueInputOption("USER_ENTERED")
                .setInsertDataOption("INSERT_ROWS")
                .setIncludeValuesInResponse(true)
                .execute();
        Log.info("Data added successfully to the google spreadsheet");
    }

    /**
     * Method to add String[] data in a google spreadsheet
     * You need to specify the place where you want to insert a new data + write the data itself to add
     * Overloaded
     */
    public void addDataInGoogleSheet(String range, String[] newData) throws IOException, GeneralSecurityException {
        Log.info("Trying to add data to the google spreadsheet");
        sheetsService = getSheetsService();
        ValueRange appendBody = new ValueRange()
                .setValues(Collections.singletonList(Arrays.asList(newData)
                ));
        appendResult(range, appendBody);
    }

    /**
     * Method to add List<List<Object>> data in a google spreadsheet
     * You need to specify the place where you want to insert a new data + write the data itself to add
     * Overloaded
     */
    public void addDataInGoogleSheet(String range, List<List<Object>> newData) throws IOException, GeneralSecurityException {
        Log.info("Trying to add data to the google spreadsheet");
        sheetsService = getSheetsService();
        ValueRange appendBody = new ValueRange()
                .setValues(newData);
        appendResult(range, appendBody);
    }

    public void copyDataInGoogleSheet(String rowToCopyFrom, String rowToCopyIn) throws IOException, GeneralSecurityException {
        sheetsService = getSheetsService();
        List<List<Object>> copiedData = getDataFromGoogleSheet(rowToCopyFrom);
        addDataInGoogleSheet(rowToCopyIn, copiedData);
    }

    /**
     * Method to update data in a google spreadsheet
     * You need to specify the place where you want to change a new data + write the data itself to change
     */
    public void updateDataInGoogleSheet(String range, String[] changedData) throws IOException, GeneralSecurityException {
        Log.info("Trying to update data in the google spreadsheet");
        sheetsService = getSheetsService();
        ValueRange body = new ValueRange()
                .setValues(Collections.singletonList(Arrays.asList(changedData)
                ));

        UpdateValuesResponse result = sheetsService.spreadsheets().values()
                .update(SPREADSHEET_ID, range, body)
                .setValueInputOption("RAW")
                .execute();
        Log.info("Data updated successfully in the google spreadsheet");
    }

    /**
     * Method to delete data from a google spreadsheet
     * You need to specify sheetID, start row and end row
     */
    public void deleteDataInGoogleSheet(int sheetID, int startRow, int endRow) throws IOException, GeneralSecurityException {
        Log.info("Trying to delete data in the google spreadsheet");
        sheetsService = getSheetsService();
        DeleteDimensionRequest deleteRequest = new DeleteDimensionRequest()
                .setRange(new DimensionRange().setSheetId(sheetID)
                        .setDimension("ROWS")
                        .setStartIndex(startRow - 1).setEndIndex(endRow) //index starts from 0, row starts from 1
                );

        List<Request> requests = new ArrayList<>();
        requests.add(new Request().setDeleteDimension(deleteRequest));

        BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);
        sheetsService.spreadsheets().batchUpdate(SPREADSHEET_ID, body).execute();
        Log.info("Data deleted successfully in the google spreadsheet");
    }
}


