package services.drive;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import enums.TypeOfDocumentGoogleDrive;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.List;

public class GoogleDriveApi {

    /**
     * Uploads a file to google drive
     * @param pathName Must be a full path to the place where we upload a file from, e.g. 'D:/some_folder/'
     */
    public void uploadFileToDrive(String fileNameToUpload, String pathName,
                                  TypeOfDocumentGoogleDrive typeOfDocumentGoogleDrive)
            throws GeneralSecurityException, IOException {
        File fileMetadata = new File();
        fileMetadata.setName(fileNameToUpload);
        java.io.File filePath = new java.io.File(pathName);
        FileContent mediaContent = new FileContent(typeOfDocumentGoogleDrive.getTypeOfDocument(), filePath);
        File file = Authorization.getService().files().create(fileMetadata, mediaContent)
                .setFields("id")
                .execute();
        System.out.println("File ID: " + file.getId());
    }

    /**
     * Prints names of files that you have on your account
     * You can decide how many files you want to see by changing @param numberOfDocumentsToPrint
     */
    public void printNamesOfFiles(int numberOfDocumentsToPrint) throws GeneralSecurityException, IOException {
        FileList result = Authorization.getService().files().list()
                .setPageSize(numberOfDocumentsToPrint)
                .setFields("nextPageToken, files(id, name)")
                .execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
            System.out.println("Files:");
            for (File file : files) {
                System.out.printf("%s (%s)\n", file.getName(), file.getId());
            }
        }
    }

    /**
     * Downloads files from Drive
     * @param destinationFolder Must be a full path to the file, e.g. 'D:/some_folder/'
     */
    public void downloadFilesFromDrive(String fileName, String destinationFolder, String fileId)
            throws IOException, GeneralSecurityException {
        OutputStream outputStream = new FileOutputStream(destinationFolder + fileName);
        Authorization.getService().files().get(fileId)
                .executeMediaAndDownloadTo(outputStream);
        outputStream.flush();
        outputStream.close();
    }
}
