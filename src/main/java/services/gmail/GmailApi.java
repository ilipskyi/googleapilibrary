package services.gmail;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.client.util.StringUtils;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Properties;

public class GmailApi extends Authorization{

    private static final Logger Log = LogManager.getLogger(GmailApi.class.getName());

    public GmailApi() throws GeneralSecurityException, IOException {
    }

    /**
     * Creates email body, but DO NOT send it to the receiver
     * @param from = me (my mailbox)
     */
    public static MimeMessage createEmail(String to, String from, String subject, String bodyText)
            throws MessagingException {

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));
        email.addRecipient(javax.mail.Message.RecipientType.TO,
                new InternetAddress(to));
        email.setSubject(subject);
        email.setText(bodyText);
        Log.info("Email successfully created");
        return email;
    }

    /**
     * Creates a message with body
     */
    private static Message createMessageWithEmail(MimeMessage emailContent)
            throws MessagingException, IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
//        String encodedEmail = com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);
        Log.info("Message with email successfully created");
        return message;
    }


    /**
     * @param userId = 'me'
     * @param emailContent contains all information of message
     */
    public void sendMessage(String userId,
                            MimeMessage emailContent)
            throws MessagingException, IOException, GeneralSecurityException {
        Message message = createMessageWithEmail(emailContent);
        Authorization authorization = new Authorization();
        message = authorization.getService().users().messages().send(userId, message).execute();

        Log.info("Message id: " + message.getId());
        Log.info(message.toPrettyString());
    }


    /**
     * @param from = 'me'
     * Sends message with attachment
     */
    public static MimeMessage createEmailWithAttachment(String to,
                                                        String from,
                                                        String subject,
                                                        String bodyText,
                                                        File file)
            throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));
        email.addRecipient(javax.mail.Message.RecipientType.TO,
                new InternetAddress(to));
        email.setSubject(subject);

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(bodyText, "text/plain");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        mimeBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(file);

        mimeBodyPart.setDataHandler(new DataHandler(source));
        mimeBodyPart.setFileName(file.getName());

        multipart.addBodyPart(mimeBodyPart);
        email.setContent(multipart);

        Log.info("Email with attachment successfully created");

        return email;
    }

    /**
     * Prints a specific message body, including sender's sign
     * Message subject of inbox: from: ...
     * Message subject of outbox: to: ...
     * @param user = 'me'
     * @param numberOfMessage if you want to get first message, this parameter must be 0
     */
    public void printMessageBody(String user, String messageSubject, int numberOfMessage) throws GeneralSecurityException, IOException {
        Authorization authorization = new Authorization();
        Gmail.Users.Messages.List llr = authorization.getService().users().messages().list(user).setQ(messageSubject);
        ListMessagesResponse listMessagesResponse = llr.execute();
        llr.setPageToken(listMessagesResponse.getNextPageToken());
        String messageID = listMessagesResponse.getMessages().get(numberOfMessage).getId();
        Message message = authorization.getService().users().messages().get(user, messageID).execute();
        String emailBody = StringUtils.newStringUtf8(com.google.api.client.util.Base64
                .decodeBase64(message.getPayload().getParts().get(numberOfMessage).getBody().getData()));
        Log.info(emailBody);
    }


            /*ListLabelsResponse listResponse = service.users().labels().list(user).execute();
            List<Label> labels = listResponse.getLabels();
            if (labels.isEmpty()) {
                System.out.println("No labels found.");
            } else {
                System.out.println("Labels:");
                for (Label label : labels) {
                    System.out.printf("- %s\n", label.getName());
                }
            }*/
//        }
}
