package test_data;

public class UploadDownloadFiles {

    private static final String downloadFileName = "retriver.jpg";
    private static final String downloadDirectory = "D:/test/";
    private static final String fileId = "1lw96YE1137Gb1UUxFpLjzpOrC9R4WU4i";

    private static final String uploadFileName = "norway_cat.jpg";
    private static final String uploadDirectory = "D:/test/";

    public static String getDownloadDirectory() { return downloadDirectory; }
    public static String getDownloadFileName() { return downloadFileName; }

    public static String getUploadFileName() { return uploadFileName; }
    public static String getUploadDirectory() { return uploadDirectory; }



    public static String getFileId() { return fileId; }

}
