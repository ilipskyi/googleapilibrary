package test_data;

public class SpreadSheetTestData {

    private static final String sheetName = "Sheet1!";

    private static final String rangeToGetDataFrom = "A4:D4";
    private static final String rangeToAddDataTo = "A7:D7";
    private static final String rangeToUpdateDataIn = "D7";
    private static final String rangeToCopyDataFrom = "A3:D3";
    private static final String rangeToCopyDataTo = "A7:D7";
    private static final String [] newPhoneModel = new String[]{"Nokia P90", "black", "5", "350$"};
    private static final String [] updatedPhonePrice = new String[]{"400$"};
    private static final int [] sheetRange = new int[]{0, 7, 8};


    public static String getSheetName() {
        return sheetName;
    }

    public static String getRangeToAddDataTo() {
        return rangeToAddDataTo;
    }

    public static String getRangeToUpdateDataIn() {
        return rangeToUpdateDataIn;
    }

    public static String getRangeToCopyDataFrom() {
        return rangeToCopyDataFrom;
    }

    public static String getRangeToCopyDataTo() {
        return rangeToCopyDataTo;
    }

    public static String[] getNewPhoneModel() {
        return newPhoneModel;
    }

    public static String[] getUpdatedPhonePrice() {
        return updatedPhonePrice;
    }

    public static int[] getSheetRange() {
        return sheetRange;
    }

    public static String getRangeToGetDataFrom() {
        return rangeToGetDataFrom;
    }

}
