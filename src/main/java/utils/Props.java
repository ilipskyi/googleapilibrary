package utils;

public class Props {

    private static final String SPREADSHEET_TOKENS_PATH = "spreadsheetTokens";
    private static final String SPREADSHEET_CREDENTIALS_PATH = "/googleSheetsCredentials.json";
    private static final String GMAIL_TOKENS_PATH = "gmailTokens";
    private static final String GMAIL_CREDENTIALS_PATH = "/gmailCredentials.json";
    private static final String GOOGLE_DRIVE_TOKENS_PATH = "googleDriveTokens";
    private static final String GOOGLE_DRIVE_CREDENTIALS_PATH = "/googleDriveCredentials.json";
    private static final String SPREADSHEET_ID = "1mf7v4oFShoKHPGP3uyt-GwOeTnS2sVVLOXH2W6nsN54";

    public static String getSpreadsheetTokensPath() {
        return SPREADSHEET_TOKENS_PATH;
    }
    public static String getSpreadsheetCredentialsPath() {
        return SPREADSHEET_CREDENTIALS_PATH;
    }
    public static String getGmailTokensPath() {
        return GMAIL_TOKENS_PATH;
    }
    public static String getGmailCredentialsPath() {
        return GMAIL_CREDENTIALS_PATH;
    }
    public static String getGoogleDriveTokensPath() {
        return GOOGLE_DRIVE_TOKENS_PATH;
    }
    public static String getGoogleDriveCredentialsPath() {
        return GOOGLE_DRIVE_CREDENTIALS_PATH;
    }
    public static String getSpreadsheetId() { return SPREADSHEET_ID; }

}
